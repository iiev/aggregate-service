package aggregateservice

object AggregateAction {
  implicit val longMonoid: Monoid[Long] = new Monoid[Long] {
    def add(x: Long, y: Long): Long = x + y
    def div(x: Long, y: Integer): Long = x / y
    def unit: Long = 0
  }

  implicit val doubleMonoid: Monoid[Double] = new Monoid[Double] {
    def add(x: Double, y: Double): Double = x + y
    def div(x: Double, y: Integer): Double = x / y
    def unit: Double = 0
  }

  def cast[A](a: Any): A = a.asInstanceOf[A]

  def sum[A](xs: List[A])(implicit m: Monoid[A]): A =
    if (xs.isEmpty) m.unit
    else m.add(xs.head, sum(xs.tail))

  def mean[A](xs: List[A])(implicit m: Monoid[A]): A =
    if (xs.isEmpty) {
      m.unit
    } else {
      val count = xs.size
      m.div(sum[A](xs), count)
    }
}
