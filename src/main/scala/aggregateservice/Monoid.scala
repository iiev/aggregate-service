package aggregateservice

abstract class Monoid[A] {
  def add(x: A, y: A): A
  def div(x: A, y: Integer): A
  def unit: A
}