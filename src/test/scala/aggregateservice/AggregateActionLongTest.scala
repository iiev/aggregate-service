package aggregateservice

import aggregateservice.AggregateAction.{doubleMonoid, longMonoid}
import org.scalatest.FunSuite

class AggregateActionLongTest extends FunSuite {

  test("AggregateAction.Long.sum") {
    val res = AggregateAction.sum[Long](List(5, 3, 8, 9, 1))
    assert(res === 26)
  }

  test("AggregateAction.Double.sum") {
    val res = AggregateAction.sum(List(12.00, 13.00, 23.42))
    assert(res === 48.42)
  }

  test("AggregateAction.Long.mean") {
    val res = AggregateAction.mean[Long](List(8, 8, 8, 8))
    assert(res === 8)
  }

  test("AggregateAction.Double.mean") {
    val res = AggregateAction.mean(List(8.00, 8.00, 8.00, 8.00))
    assert(res === 8.00)
  }
}
